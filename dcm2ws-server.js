﻿var wsport = 543;
var tcpport = 1112;
var WebSocketServer = require('ws').Server;
var wss = new WebSocketServer({port: wsport});

var wsClient;
var tcpsocket;

var buffer = new Buffer(0);
var first = true;
var pduLen = 0;
var copyLen = 0;
var timeout = null;

var firstSend = false;
var slicePos = 0;
var sumReceived = 0;

function CleanTemp() {
    buffer = new Buffer(0);
    first = true;
    pduLen = 0;
    copyLen = 0;
}

wss.on('connection', function(wsclient) 
{
    wsClient = wsclient;
    
    wsclient.on('message', function(data) 
    {        
        //console.log("-------------------------------");
        //console.log("Sending to listener " + data.length + " bytes");
        //console.log("-------------------------------");
        tcpsocket.write(data);
        //CleanTemp();
        
    });
        
    console.log("-------------------------------");
    console.log("WS Client connected");    
});

console.log("WS Server started at port " + wsport);

//***** TCP server listener

// Load the net module to create a tcp server.
var net = require('net');

// Setup a tcp server
var server = net.createServer({ allowHalfOpen: false }, function (listener)
{
    tcpsocket = listener;

    // Every time someone connects, tell them hello and then close the connection.
    listener.addListener("connect", function ()
    {
        console.log("---------Begin----------------");
        console.log("TCP Connection from " + listener.remoteAddress);
        
        tcpsocket = listener;

    });
  
    listener.addListener("data", function (data)
    {

        var dataLength = data.length;

        buffer = Buffer.concat([buffer, data]);
        
        if (first) {
            pduLen = buffer.readUInt32BE(2) + 6;            
            first = false;
        }        
        copyLen += dataLength;

        var bufferLength = buffer.length;
        if (buffer.length >= pduLen) {                        
            var pale = new Buffer(bufferLength - pduLen);
            buffer.copy(pale, 0, pduLen, bufferLength);
            var buf = buffer.slice(0, pduLen);
            buffer = pale;
            wsClient.send(buf, { binary: true, mask: false });
            if (first) first = false;
            first = true;
            copyLen -= pduLen;

            //NOT OPTIMIZED
            if (copyLen > 0) {
                var tmpPduLen = buffer.readUInt32BE(2) + 6;
                if (tmpPduLen == buffer.length) {
                    wsClient.send(buffer, { binary: true, mask: false });
                    //console.log("Residual sent");
                }
            }

        }            
    
    });

    listener.addListener("end", function () {

        console.log("----------end----------------");
        CleanTemp();

    });

    listener.addListener("close", function (e) {
        CleanTemp();
    });
  
    //error hendling
    listener.addListener("error", function (e)
    {
        console.log("error, code=" + e.code);

        if(e.code == "EPIPE"){
            //server.close();
            //server.listen(104, "localhost");
            //return;
        }

        CleanTemp();

        if (e.code == 'EADDRINUSE')
        {
            console.log('Address in use, retrying...');
            setTimeout(function () 
            {
                server.close();
                server.listen(104, "localhost");
            }, 1000);
        }
    });
        
    });

    // Fire up the server bound to port 104 on localhost
    server.listen(tcpport, "0.0.0.0");
    console.log("TCP Server listen at port " + tcpport);