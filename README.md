# DICOM to the WebSocket proxy#

Simple Node.js **proof-of-concept proxy** that transfers DICOM TCP packets to the WebSocket protocol in order to use it with JavaScript DICOM network library.

### What is inside? ###
* dcm2ws-client.js - DICOM client only proxy, acts as a C-ECHO SCU, C-FIND SCU, C-MOVE SCU
* dcm2ws-server.js - DICOM server proxy, acts as a C-ECHO SCP, C-STORE SCP
* dcm2ws.js - DICOM server and client in one package - not so stable

### How do I get set up? ###

* First of all you need DICOM device - PACS, workstation, modality...
* Download repo on the machine in the same LAN as DICOM device
* Install Node.js - https://nodejs.org/en/
* Install ws library - https://github.com/websockets/ws
* Change AET or port of proxy if you want, default is *JavaScript* and *1112*
* Put those parameters in DICOM device
* Change WebSocket port if you want, default is *543*
* Run proxy: 
```
#!javascript

node dcm2ws.js
```
* Now use JADO library - https://bitbucket.org/willy_skipper/javacriptaccesstodicomobjects