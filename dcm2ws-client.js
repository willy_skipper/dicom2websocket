﻿var wsport = 543;
var tcpport = 1112;
var net = require('net');
var WebSocketServer = require('ws').Server;
var wss = new WebSocketServer({port: wsport});
var wsClient;
var tcpListener;
var wsocket;

//pomocne vars
var buffer = new Buffer(0);
var first = true;
var pduLen = 0;
var copyLen = 0;


wss.on('connection', function(wsclient) 
{
    wsClient = wsclient;
    
    wsclient.on('message', function(data) 
    {
        if ((typeof data) == "string")
        {
            var foo = JSON.parse(data);
            console.log(typeof data);
            console.log(foo.port);
            console.log(foo.host);

            //console.log("Data = " + foo.data.length);

            if (foo.function == "connect")
            {
                wsocket = net.connect({ port: foo.port, host: foo.host }, function () {
                    //'connect' listener
                    console.log('wsocket: client connected');
                    //wsocket.write('world!\r\n');                      
                });

                wsocket.on('data', function (data) {                    
                    console.log('-------------- socket data received ----------------');
                    console.log("Recieved " + data.length + " bytes");
                    console.log(data.toString());

                    ProcessPDU(data);
                });

                wsocket.on('end', function () {
                    console.log('wsocket: client disconnected');
                });

                wsocket.on('error', function (e) {
                    console.log('wsocket: error ' + e);
                });

            }
            else if (foo.function == "close")
            {
                console.log("socket close");
                wsocket.end();
            }

        }
        else {
            console.log("---------------- Sending data to client ------------------- ");
            console.log("Bytes = " + data.length);
            wsocket.write(data);
        }

        
    });
        
    console.log("-------------------------------");
    console.log("Client connected");    
});

console.log("WS Server started at port " + wsport);

function ProcessPDU(data)
{
    var dataLength = data.length;

    buffer = Buffer.concat([buffer, data]);

    if (first) {
        pduLen = buffer.readUInt32BE(2) + 6;
        console.log("PDU len = " + pduLen);
        first = false;
    }

    copyLen += dataLength;

    var bufferLength = buffer.length;
    if (buffer.length >= pduLen) {

        var pale = new Buffer(bufferLength - pduLen);

        buffer.copy(pale, 0, pduLen, bufferLength);

        var buf = buffer.slice(0, pduLen);

        buffer = pale;
     
        wsClient.send(buf, { binary: true, mask: false });
        console.log("Sending to websocket " + buf.length + " bytes");

        if (first) first = false;
        first = true;
        copyLen -= pduLen;

        if (copyLen > 0)
        {
            var tmpPduLen = buffer.readUInt32BE(2) + 6;
            if (tmpPduLen == buffer.length)
            {
                wsClient.send(buffer, { binary: true, mask: false });
                console.log("Residual sent");
            }
            else if (tmpPduLen < buffer.length)
            {
                buffer = new Buffer(0);
                first = true;
                pduLen = 0;
                copyLen = 0;
                ProcessPDU(pale);
                
            }
        }
    }
}

