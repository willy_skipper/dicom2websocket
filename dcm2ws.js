var wsport = 543;
var tcpport = 1112;
var WebSocketServer = require('ws').Server;
var wss = new WebSocketServer({port: wsport});
var socket;

var wsClient;
var tcpSocket;

var buffer = new Buffer(0);
var first = true;
var pduLen = 0;
var copyLen = 0;
var tajmout = null;

var firstSend = false;
var slicePos = 0;
var sumIncoming = 0;

var isConnected = false;
var isSCUConnected = false;

function Clean() {
    buffer = new Buffer(0);
    first = true;
    pduLen = 0;
    copyLen = 0;
}

wss.on('connection', function(wsclient) 
{
    wsClient = wsclient;
    
    wsclient.on('message', function(data) 
    {
        if ((typeof data) == "string")
        {
            var foo = JSON.parse(data);
            console.log(typeof data);
            console.log(foo.port);
            console.log(foo.host);

            //console.log("Data = " + foo.data.length);

            if (foo.function == "connect")
            {
                socket = net.connect({ port: foo.port, host: foo.host }, function () {
                    //'connect' listener
                    console.log('socket: client connected');
                    isSCUConnected = true;                  
                });

                socket.on('data', function (data) {                    
                    console.log('-------------- socket data received ----------------');
                    console.log("Recieved " + data.length + " bytes");
                    console.log(data.toString());

                    ProcessPDU(data);
                });

                socket.on('end', function () {
                    console.log('socket: client disconnected');
                    //isSCUConnected = false;
                });

                socket.on('error', function (e) {
                    console.log('socket: error ' + e);
                    isSCUConnected = false;
                });

            }
            else if (foo.function == "close")
            {
                isSCUConnected = false;
                console.log("socket close");
                socket.end();
            }

        }
        else 
        {
            console.log("---------------- Saljem klijentu podatke ------------------- ");
            console.log("Bytes = " + data.length);
            
            if(isSCUConnected){
                console.log("SCU socket Sending");
                socket.write(data);     
                return;   
            }

            if(isConnected){
                console.log("tcpSocket Sending");
                tcpSocket.write(data);
            }

        }
        
    });
        
    console.log("-------------------------------");
    console.log("WS Client connected");    
});

console.log("WS Server started at port " + wsport);

//***** TCP server listener

// Load the net module to create a tcp server.
var net = require('net');

// Setup a tcp server
var server = net.createServer({ allowHalfOpen: true,  }, function (listener)
{
    //ovo negdje treba, a negdje ne
    tcpSocket = listener;

    //tcpSocket.setKeepAlive(true,60000);
    //tcpSocket.setNoDelay(false);

    // Every time someone connects, tell them hello and then close the connection.
    listener.addListener("connect", function ()
    {
        isConnected = true;
        console.log("---------Begin----------------");
        console.log("TCP Connection from " + listener.remoteAddress);
        
        tcpSocket = listener;

    });
  
    listener.addListener("data", function (data)
    {
 
        var dataLength = data.length;
        console.log("Recived " + dataLength + " bytes");

        buffer = Buffer.concat([buffer, data]);

        if (first) {
            pduLen = buffer.readUInt32BE(2) + 6;
            first = false;
        }        

        copyLen += dataLength;

        var bufferLength = buffer.length;
        if (buffer.length >= pduLen) {            

            var pale = new Buffer(bufferLength - pduLen);
            
            buffer.copy(pale, 0, pduLen, bufferLength);
           
            var buf = buffer.slice(0, pduLen);
          
            buffer = pale;
         
            console.log("sending to browser " + buf.length + " bytes");
            wsClient.send(buf, { binary: true, mask: false});
     
            if (first) first = false;
            first = true;

            copyLen -= pduLen;

            if (copyLen > 0) {
                var tmpPduLen = buffer.readUInt32BE(2) + 6;
                if (tmpPduLen == buffer.length) {
                    wsClient.send(buffer, { binary: true, mask: false });
                }
            }
        }
    
    });

    listener.addListener("end", function (e) {

        //isConnected = false;
        console.log("----------end----------------");
        console.log("error, code=" + e);
        Clean();

    });

    listener.addListener("close", function (e) {
        console.log("----------closed----------------");
        console.log("error, code=" + e);
        Clean();
    });
  
    listener.addListener("error", function (e)
    {
        console.log("----------error----------------");
        console.log("error, code=" + e.code);
        Clean();
        isConnected = false;

        if (e.code == 'EADDRINUSE')
        {
            console.log('Address in use, retrying...');
            setTimeout(function () 
            {
                server.close();
                server.listen(tcpport, "0.0.0.0");
                isConnected = true;
            }, 1000);
        }
    });
        
});

    // Fire up the server bound to port 104 on localhost
server.listen(tcpport, "0.0.0.0");
isConnected = true;
console.log("TCP Server listen at port " + tcpport);


function ProcessPDU(data)
{
    var dataLength = data.length;
  
    buffer = Buffer.concat([buffer, data]);

    //3. kolika je poruka
    if (first) {
        pduLen = buffer.readUInt32BE(2) + 6;
        console.log("PDU len = " + pduLen);
        first = false;
    }

    copyLen += dataLength;
   
    var bufferLength = buffer.length;
    if (buffer.length >= pduLen) {
       
        var pale = new Buffer(bufferLength - pduLen);
        
        buffer.copy(pale, 0, pduLen, bufferLength);
        
        var buf = buffer.slice(0, pduLen);
        
        buffer = pale;
        
        wsClient.send(buf, { binary: true, mask: false });
        console.log("Saljem na websocket " + buf.length + " bytes");
        
        if (first) first = false;
        first = true;
        
        copyLen -= pduLen;
        
        
        if (copyLen > 0)
        {
            var tmpPduLen = buffer.readUInt32BE(2) + 6;
            if (tmpPduLen == buffer.length)
            {
                wsClient.send(buffer, { binary: true, mask: false });
                console.log("Residual sent");
            }
            else if (tmpPduLen < buffer.length)
            {
                buffer = new Buffer(0);
                first = true;
                pduLen = 0;
                copyLen = 0;
                ProcessPDU(pale);
                
            }
        }
    }
}